import React from 'react';
import ReactTestUtils from 'react-dom/test-utils'
import Board from '../Board.js'
// import {handleClick} from '../Board.js'
// import {renderSquare} from '../Board.js'


describe('Board constructor tests', ()=>{

// 
    test('Is initial state true?', ()=>{
        let component = ReactTestUtils.renderIntoDocument(<Board/>);
        let state = component.state;
        expect(state.xIsNext).toBe(true);
    }); 

// This test passes if at least one of the state.squares has been initialized to null. 
    test('Passes if at least one of the initial squares is null?', ()=>{
        const expected = [null];
        let component = ReactTestUtils.renderIntoDocument(<Board/>);
        let state = component.state;
        expect(state.squares).toEqual(expect.arrayContaining(expected));
    }); 

// This test passes if state.squares contain neither an X or an O.
    test('Passes if none of the initial squares as an X or O', ()=>{
        const expected = ['X','O'];
        let component = ReactTestUtils.renderIntoDocument(<Board/>);
        let state = component.state;
        expect(state.squares).toEqual(expect.not.arrayContaining(expected));
    }); 
    
    // This test passes if all of the state.squares are null.
    test('Passes if all the initial squares are null', ()=>{
        const expected =  [null];
        let component = ReactTestUtils.renderIntoDocument(<Board/>);
        let state = component.state;
        expect(state.squares).not.toEqual(expect.not.arrayContaining(expected));
    }); 
    
}); // describe

describe('Board Component (html) tests', ()=>{
    // Are there any 'div' tags produced by the Board component?
    test('Does Board have any button tags?', ()=>{
        const component = ReactTestUtils.renderIntoDocument(<Board/>);
        let div = ReactTestUtils.scryRenderedDOMComponentsWithTag(component,'button');   
       
    }); 

    // Does Board have a status class?
        test('Does Board have one status class?', ()=>{
        const component = ReactTestUtils.renderIntoDocument(<Board/>);
        let div = ReactTestUtils.findRenderedDOMComponentWithClass(component,'status');   
       
    }); 
 
});


